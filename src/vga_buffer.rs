#[allow(dead_code)]  // 不对未使用变量给出警告
#[derive(Debug, Clone, Copy, PartialEq, Eq)]  // 类型遵循复制、比较、调试、打印语义
#[repr(u8)]  // 颜色枚举值u4即可，但rust不提供u4类型
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2, 
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10, 
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]  // 类型遵循复制、比较、调试、打印语义
#[repr(transparent)]  // 和u8有完全相同的内存布局
struct ColorCode(u8);

impl ColorCode {
    fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

// 字符缓冲区的每个字符，由输出字符和颜色代码组成
#[derive(Debug, Clone, Copy, PartialEq, Eq)]  // 类型遵循复制、比较、调试、打印语义
#[repr(C)]  // 按照C语言约定：按顺序布局成员变量（Rust不一定按顺序布局）
struct ScreenChar {
    ascii_character: u8,
    color_code: ColorCode,
}

// 字符缓冲区，指定宽度和高度
const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;


use volatile::Volatile;  
struct Buffer {  // 只能通过write写入，而不会被编译器优化的操作写入
    chars: [[Volatile<ScreenChar>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}


// 将字符缓冲区输出到屏幕
pub struct Writer {
    column_position: usize,  // 最后一行的光标的位置
    color_code: ColorCode,
    buffer: &'static mut Buffer,  // 'static显式生命周期：运行期间该引用变量全局有效
}

impl Writer {
    pub fn write_byte(&mut self, byte: u8) {
        match byte { // 类似switch语句
            b'\n' => self.new_line(),  // 遇到\n，Writer换行
            byte => {
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();  // 这一行打印满了，Writer换行
                }
                let row = BUFFER_HEIGHT - 1;
                let col = self.column_position;
                let color_code = self.color_code;
                self.buffer.chars[row][col].write(ScreenChar {
                    ascii_character: byte,
                    color_code,
                });
                self.column_position += 1;
            }
        }
    }

    pub fn write_string(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                _ => self.write_byte(0xfe),  // 超出范围的字符输出实心正方形
            }
        }
    }

    // 换行时将所有字符上移一行，并在最后一行起始继续打印
    fn new_line(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let character = self.buffer.chars[row][col].read();
                self.buffer.chars[row - 1][col].write(character);
            }
        }
        self.clear_row(BUFFER_HEIGHT - 1);
        self.column_position = 0;
    }

    // 清空行可以用填充一行空格实现
    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_character: b' ',
            color_code: self.color_code,
        };
        for col in 0..BUFFER_WIDTH {
            self.buffer.chars[row][col].write(blank);
        }
    }
}

// 全局使用的静态变量，默认时编译时定值，需要懒加载（到第一次使用时定值）
use spin::Mutex;  // Writer写入时使用自旋互斥锁
use lazy_static::lazy_static;
lazy_static! {
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer {
        column_position: 0,
        color_code: ColorCode::new(Color::Yellow, Color::Black),
        buffer: unsafe{ &mut *(0xb8000 as *mut Buffer) },
    });
}

use core::fmt;  // 使用Rust格式化宏，以打印不同类型的变量

impl fmt::Write for Writer {  // 重写方法，和write_string差别只是返回值类型变化了
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())  // Result枚举类型中的OK，包含值为()的变量
    }
}

// 定义两个宏，使得能在整个crate使用同时保证安全，已经占用use crate::println
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

#[doc(hidden)]  // 这是私有的实现细节，防止在生成文档中出现
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;
    // 允许无中断环境执行，避免死锁
    interrupts::without_interrupts(|| {
        WRITER.lock().write_fmt(args).unwrap();
    });
}


#[test_case]
fn test_println_output() {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;

    let s = "Some test string that fits on a single line";
    // 无中断环境下输出避免死锁
    interrupts::without_interrupts(|| {
        // 使用writeln宏而非println以绕开输出必须加锁的限制
        let mut writer = WRITER.lock();
        writeln!(writer, "\n{}", s).expect("writeln failed.");
        for (i, c) in s.chars().enumerate() {
            let screen_char = WRITER.lock().buffer.chars[BUFFER_HEIGHT - 2][i].read();
            // 比较屏幕上字符和c是否一致，以确认是否确实出现在VGA文本缓冲区中
            assert_eq!(char::from(screen_char.ascii_character), c);
        }
    });
}