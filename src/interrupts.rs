// 处理CPU中断，IDT即中断描述符表

use x86_64::structures::idt::{
    InterruptDescriptorTable, 
    InterruptStackFrame, 
    PageFaultErrorCode};
use crate::{gdt, println};
use lazy_static::lazy_static;
use pic8259::ChainedPics;
use spin;
use crate::print;
use crate::hlt_loop;

// 重新映射主副PIC序号（32-47），因为前面的被CPU异常编号占用了
pub const PIC_1_OFFSET: u8 = 32;
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

pub static PICS: spin::Mutex<ChainedPics> =
    spin::Mutex::new(unsafe {
       ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) 
    });

// idt生命周期应当是整个程序生命周期，因此是全局static
lazy_static! {
    static ref IDT:InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        idt.breakpoint.set_handler_fn(breakpoint_handler);
        unsafe {
            idt.double_fault.set_handler_fn(double_fault_handler)
            .set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX);  // 指定到IST的安全栈序号（不安全）
        };
        idt[InterruptIndex::Timer.as_usize()]
            .set_handler_fn(timer_interrupt_handler);
        idt[InterruptIndex::Keyboard.as_usize()]
            .set_handler_fn(keyboard_interrupt_handler);
        idt.page_fault.set_handler_fn(page_fault_handler);
        idt
    };
}
    

pub fn init_idt() {
    IDT.load();  // 让CPU使用新的IDT，调用lidt指令
}

// 断点异常（断点调试常用）
// x86-interrupt功能不稳定，须在lib.rs启用#![feature(abi_x86_interrupt)]
extern "x86-interrupt" fn breakpoint_handler(
    stack_frame: InterruptStackFrame) {
    println!("EXCEPTION: BREAKPOINT\n{:#?}", stack_frame);
}

// 双重异常（执行异常处理函数失败时触发）
// 这是一个发散函数，因为x86_64不允许从double fault返回任何东西
extern "x86-interrupt" fn double_fault_handler(
    stack_frame: InterruptStackFrame, _error_code: u64) -> ! {
    panic!("EXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
}

// 计时器中断处理
extern "x86-interrupt" fn timer_interrupt_handler(
    _stack_frame: InterruptStackFrame)
{
    print!(".");
    // 处理函数需要返回中断结束（EOI）信号，系统才准备接收下一个中断
    // 中断信号不能错，不然导致该信号迟迟得不到回应使得系统挂起
    unsafe {
        PICS.lock().notify_end_of_interrupt(InterruptIndex::Timer.as_u8());
    }
}

// 键盘中断处理
extern "x86-interrupt" fn keyboard_interrupt_handler(
    _stack_frame: InterruptStackFrame)
{
    // 配置键盘译码器
    use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
    use spin::Mutex;
    lazy_static! {
        static ref KEYBOARD: Mutex<Keyboard<layouts::Us104Key, ScancodeSet1>> =
            Mutex::new(Keyboard::new(layouts::Us104Key, ScancodeSet1,
                HandleControl::Ignore)
            );
    }
    let mut keyboard = KEYBOARD.lock();
    
    // 从键盘控制器的IO端口（0x60）获取按键扫描码
    use x86_64::instructions::port::Port;
    let mut port = Port::new(0x60);
    let scancode: u8 = unsafe { port.read() };
    if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
        if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(character) => print!("{}", character),
                DecodedKey::RawKey(key) => print!("{:?}", key),
            }
        }
    }

    unsafe {
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Keyboard.as_u8());
    }
}

extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    // 页错误发生时，CPU自动写入异常虚拟地址到CR2寄存器
    use x86_64::registers::control::Cr2;

    println!("EXCEPTION: PAGE FAULT");
    println!("Accessed Address: {:?}", Cr2::read());
    println!("Error Code: {:?}", error_code);
    println!("{:#?}", stack_frame);
    hlt_loop();  // 需要显式结束异常处理，才能恢复程序运行
}

#[test_case]
fn test_breakpoint_exception() {
    // 唤起一个断点异常
    x86_64::instructions::interrupts::int3();
}

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = PIC_1_OFFSET,  // 计时器组件使用主PIC的0号管脚
    Keyboard,  // 无需显式指定值，因为自动指定中断编号为上一个+1
}

impl InterruptIndex {
    fn as_u8(self) -> u8 {
        self as u8
    }

    fn as_usize(self) -> usize {
        usize::from(self.as_u8())
    }
}




