// 全局描述符表
use x86_64::VirtAddr;
use x86_64::structures::tss::TaskStateSegment;  // 任务状态段(TSS)
use x86_64::structures::gdt::{GlobalDescriptorTable, Descriptor, SegmentSelector};
use lazy_static::lazy_static;

pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;  // IST中安全栈序号


// x86_64架构TSS包含特权栈表、中断栈表(IST)、I/O映射基准地址
lazy_static! {
    static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();
        // 发生Double Fault时，交给IST内的一个栈
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = 4096 * 5;
            // 暂用数组模拟栈，之后用真正的栈分配代替
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];

            let stack_start = VirtAddr::from_ptr(unsafe { &STACK });
            let stack_end = stack_start + STACK_SIZE;
            stack_end
        };
        tss
    };
}

// GDT可用于切换内核空间和用户空间，以及加载TSS结构
lazy_static! {
    static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();
        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        let tss_selector = gdt.add_entry(Descriptor::tss_segment(&TSS));
        (gdt, Selectors { code_selector, tss_selector })
    };
}

struct Selectors {
    code_selector: SegmentSelector,
    tss_selector: SegmentSelector,
}

pub fn init() {
    use x86_64::instructions::tables::load_tss;
    // 代码段寄存器CS
    use x86_64::instructions::segmentation::{CS, Segment};

    // 重载代码段寄存器CS和TSS
    GDT.0.load();  // 0对应GDT，1对应Selectors
    unsafe {
        CS::set_reg(GDT.1.code_selector);
        load_tss(GDT.1.tss_selector);
    }
}