#![no_std]  // 禁止链接标准库，需要自定义panic_handler和eh_personality语言项
// eh_personality语言项于panic发生时执行栈展开，本OS通过toml直接禁止栈展开
#![no_main]  // 禁止默认的程序入口main
// #![feature(asm)]  // 内联汇编默认开启
// 由于no_std，无法使用#[test]需要自定义测试框架
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
// 改变测试时生成函数名称防止测试时duplicate language item
#![reexport_test_harness_main = "test_main"]


mod serial;  // test通过串口模块写到控制台上


// 禁止标准库后要自定义不可恢复处理函数
use core::panic::PanicInfo;

#[cfg(not(test))]  // 非test模式下的Panic处理
#[panic_handler]  // 属性宏，指定下述函数为panic_handler
// 发散函数：!表示不会正常返回，只会退出程序或者无限循环
fn panic(info: &PanicInfo) -> ! { 
    println!("{}", info);
    felios::hlt_loop();
    loop {}
}

#[cfg(test)]  // test模式下的Panic处理（通过串口输出到控制台）
#[panic_handler]  // 属性宏，指定下述函数为panic_handler
// 发散函数：!表示不会正常返回，只会退出程序或者无限循环
fn panic(info: &PanicInfo) -> ! { 
    felios::test_panic_handler(info);
}


mod vga_buffer;  // 引入文件模块
use bootloader::{BootInfo, entry_point};

entry_point!(kernel_main);  // 告知bootloader的入口Rust函数
fn kernel_main(boot_info: &'static BootInfo) -> ! {
    println!("Welcome to FeliOS\nversion: 0.1.0\nauthor: GuoCheng");
    use felios::memory;
    use x86_64::{
        structures::paging::{Translate, Page}, 
        VirtAddr
    };
    felios::init();

    // 从boot_info中读取到虚拟地址映射偏移
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe {
        memory::BootInfoFrameAllocator::init(&boot_info.memory_map)
    };
    
     // 映射未使用的页
     let page = Page::containing_address(VirtAddr::new(0));
     memory::create_example_mapping(page, &mut mapper, &mut frame_allocator);
     // 通过新的映射将字符串 `New!`  写到屏幕上。
     let page_ptr: *mut u64 = page.start_address().as_mut_ptr();
     unsafe { page_ptr.offset(400).write_volatile(0x_f021_f077_f065_f04e)};

    // VA->PA测试
    let addresses = [
        // the identity-mapped vga buffer page
        0xb8000,
        // some code page
        0x201008,
        // some stack page
        0x0100_0020_1a10,
        // virtual address mapped to physical address 0
        boot_info.physical_memory_offset,
    ];
    for &address in &addresses {
        let virt = VirtAddr::new(address);
        let phys = mapper.translate_addr(virt);
        println!("{:?} -> {:?}", virt, phys);
    }
    
    
    
    // let l4_table = unsafe { active_level_4_table(phys_mem_offset) };
    // use x86_64::structures::paging::PageTable;
    // // 输出四级页表条目
    // for (i, entry) in l4_table.iter().enumerate() {
    //     if !entry.is_unused() {
    //         // 输出三级表项目
    //         let phys = entry.frame().unwrap().start_address();
    //         let virt = phys.as_u64() + boot_info.physical_memory_offset;
    //         let ptr = VirtAddr::new(virt).as_mut_ptr();
    //         let l3_table: &PageTable = unsafe { &*ptr };
    //         let mut used_l3_cnt = 0;
    //         for (i, entry) in l3_table.iter().enumerate() {
    //             if !entry.is_unused() {
    //                 used_l3_cnt += 1;
    //             }
    //         }
    //         println!("L4 Entry {}: {:?}, l3 entry cnt: {}", i, entry, used_l3_cnt);
    //     }
    // }

    use x86_64::registers::control::Cr3;
    // CR3寄存器存储4级页表指针（物理地址）及其标志位
    let (level_4_page_table, _) = Cr3::read();
    println!("Level 4 page table at: {:?}", level_4_page_table.start_address());

    // 唤起页错误
    // unsafe {
    //     // 根据x86_64虚拟地址规范可得知：
    //     // 0x2031b2代码页，可读不可写；0xdeadbeaf不可读不可写
    //     let ptr = 0x2031b2 as *mut u8;
    //     let x = *ptr;
    //     println!("read success!");
    //     *ptr = 42;
    // }
    
    // 唤起一个断点异常
    // x86_64::instructions::interrupts::int3();
    
    // 唤起double fault
    // fn stack_overflow() {
    //     stack_overflow();
    // }
    // stack_overflow();


    #[cfg(test)]
    test_main();
    // panic!("Some panic!");
    println!("FeliOS intialize success!");

    // felios::hlt_loop();
    loop {
        // use felios::print;
        // for _ in 0..100000 {
        // }
        // print!("-");
    }
}


#[cfg(test)]  // 令下面的函数仅出现在测试中，不测试时无用
pub fn test_runner(tests: &[&dyn Testable]) {
    serial_println!("Running {} tests", tests.len());
    for test in tests {
        test.run();
    }
    // 测试结束退出qemu，并将测试输出打印到控制台
    exit_qemu(QemuExitCode::Success);
}


#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}


// 结束测试退出QEMU
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    // 只要不和QEMU退出代码冲突即可（例:1是QEMU默认退出代码）
    Success = 0x10,
    Failed = 0x11,
}

pub fn exit_qemu(exit_code: QemuExitCode) {
    use x86_64::instructions::port::Port;

    unsafe {
        // 0xf4表示未使用的端口，也是isa-debug-exit的IO端口
        let mut port = Port::new(0xf4);
        port.write(exit_code as u32);
    }
}


// 测试时自动添加打印语句，避免多个测试case重复实现输出
pub trait Testable {  // trait 公共特性（类似于抽象接口）
    fn run(&self) -> ();
}

impl<T> Testable for T  // 为类型T实现Testable
where T:Fn(),  // 约束条件，仅当类型T实现了Fn()特性，才使用下述实现，Fn()表示可被调用的包或函数
{
    fn run(&self) {
        // 输出函数名
        serial_print!("{}...\t", core::any::type_name::<T>());
        self();  // 调用测试函数本身，该方式Fn() trait专属
        serial_println!("[ok]");   
    }
}