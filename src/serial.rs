// 自建串口模块，而不是用uart_16650自带的SerialPort实现
use uart_16550::SerialPort;  // 代表UART寄存器
use spin::Mutex;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref SERIAL1: Mutex<SerialPort> = {
        // 0x3f8时第一个串行接口的标准端口号
        let mut serial_port = unsafe { SerialPort::new(0x3F8) };
        serial_port.init();  // lazy_static使得仅在第一次使用时init
        Mutex::new(serial_port)
    };
}

#[doc(hidden)]
pub fn _print(args: ::core::fmt::Arguments) {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;
    // 允许无中断环境执行，避免死锁
    interrupts::without_interrupts(|| {
        SERIAL1.lock().write_fmt(args)
            .expect("Printing to serial failed");
    });
}

// #[macro_export]使得接下来的macro_rules宏直接位于根命名空间下，无需导入宏

/// Prints to the host through the serial interface.
#[macro_export]
macro_rules! serial_print {
    ($($arg:tt)*) => {
        $crate::serial::_print(format_args!($($arg)*));
    };
}

/// Prints to the host through the serial interface, appending a newline.
#[macro_export]
macro_rules! serial_println {
    () => ($crate::serial_print!("\n"));
    ($fmt:expr) => ($crate::serial_print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => ($crate::serial_print!(
        concat!($fmt, "\n"), $($arg)*));
}