# Intro
Felios基于 https://os.phil-opp.com/ 教程开发，一个基于x86的裸机(Bare-Metal)操作系统，适合嵌入式。

# How to build?
执行 cargo build

# How to run the OS on qemu?
执行 cargo run；
如果没有安装qemu，Windows平台从官网下载安装qemu后，将qemu的安装文件夹路径加入到系统环境变量的Path中，然后重启电脑即可。

# How to test?
执行 cargo test，引入单元测试和集成测试。
集成测试（参见tests下的文件名）：
cargo test --test basic_boot
cargo test --test should_panic
单元测试均复用自src/lib.rs中的test_handler，执行所有单元测试：
cargo test --lib
