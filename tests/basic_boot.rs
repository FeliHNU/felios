// 集成测试都是单独的文件（有利于完全控制环境），与main.rs完全独立
// 同时不需要cfg(test)，因为非测试环境不会编译集成测试文件
#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![reexport_test_harness_main = "test_main"]
// 调用lib.rs中定义的test_runner
#![test_runner(felios::test_runner)]


use core::panic::PanicInfo;

#[no_mangle] // don't mangle the name of this function
pub extern "C" fn _start() -> ! {
    test_main();

    loop {}
}


#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    // 调用lib.rs中的函数
    felios::test_panic_handler(info)
}

use felios::println;
#[test_case]
fn test_println() {
    println!("test_println output");
}