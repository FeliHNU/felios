#![no_std]
#![no_main]
#![feature(abi_x86_interrupt)]

use core::panic::{self, PanicInfo};
use felios::serial_print;
use lazy_static::lazy_static;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use felios::{exit_qemu, QemuExitCode, serial_println};


#[no_mangle]
pub extern "C" fn _start() -> ! {
    serial_print!("stack_overflow::stack_overflow...\t");
    felios::gdt::init();
    init_test_idt();
    stack_overflow();
    panic!("Execution continued after stack overflow");
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    felios::test_panic_handler(info)
}

// 关闭编译器对递归的警告
#[allow(unconditional_recursion)]
fn stack_overflow() {
    stack_overflow();
    // 阻止编译器进行尾调用优化（降级为循环）
    volatile::Volatile::new(0).read();
}

// 和interrupts.IDT区别在于处理函数本身
lazy_static! {
    static ref TEST_IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        // 比interrupts.IDT少breakpoint_handler
        unsafe {
            idt.double_fault
                .set_handler_fn(test_double_fault_handler)
                .set_stack_index(felios::gdt::DOUBLE_FAULT_IST_INDEX);
        }

        idt
    };
}

pub fn init_test_idt() {
    TEST_IDT.load();
}


// 和interrupts.IDT不同的核心原因——处理函数将其视为成功的，表示测试完成
extern "x86-interrupt" fn test_double_fault_handler(
    _stack_frame: InterruptStackFrame,
    _error_code: u64,
) -> ! {
    serial_println!("[ok]");
    exit_qemu(QemuExitCode::Success);
    loop {}
}